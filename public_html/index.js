//IIFF
(function () { 
	var PlayerDraftApp = angular.module("PlayerDraftApp", ['ui-rangeSlider']);

        PlayerDraftApp.config(['$httpProvider', function ($httpProvider) {
            $httpProvider.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
            }]);
        
         var PlayerSvc = function($http, $q) {
               
               //https://api.myjson.com/bins/14qdrz"; http://localhost:3000/players;
               urltest = "http://localhost:3000/players";
               getfile = "playerData.json"; 
               this.getPlayer = function() {
                    var defer = $q.defer();
                    $http.get(getfile).then(function(result) {
                             defer.resolve(result.data.players);
                             //console.info(result.data);
                        }).catch(function(error) {
                            defer.reject(error);
                        });
                        return (defer.promise);
                };
            
        };
       
        var PlayerDraftCtrl = function($http, $scope, PlayerSvc) {
            var playerDraftCtrl = this; 
           
            PlayerDraftCtrl.ako = [];
            $scope.index = -1;
            
            playerDraftCtrl.playr = PlayerSvc.getPlayer();
            //console.log(playerDraftCtrl.playr);
            
            PlayerSvc.getPlayer()
            .then(function(data) {
                playerDraftCtrl.data = data;
                //console.info(JSON.stringify(data));
                $scope.players = data;
                
            }).catch(function(error) {
                    console.error(">>> error: %s ", error);
                });
           
            playerDraftCtrl.rangeSample = { 
                range: { 
                    min: 1000000,
                    max: 10000000
                },
                minPrice: 1000000,
                maxPrice: 5000000
            };
            //console.log(playerDraftCtrl.rangeSample.range.max); 
           
           $scope.dataObj = {
                                id: $scope.id,
                                fname: $scope.fname,
                                lname: $scope.lname,
                                salary: $scope.salary,
                                points: $scope.points,
                                rebounds: $scope.rebounds,
                                assists: $scope.assists,
                                steals: $scope.steals,
                                blocks: $scope.blocks
                                };
                                
                //Add playter method
            playerDraftCtrl.addPlayer = function() {
                  
                    $scope.fname='';
                    $scope.lname='';
                    $scope.salary='';
                    $scope.points='';
                    $scope.rebounds='';
                    $scope.assists='';
                    $scope.steals='';
                    $scope.blocks='';
                    
                    PlayerDraftCtrl.ako.push(); 
                    url = urltest;
                    
                    var config = {
                         headers : {
                             'Content-Type': 'application/json;'
                         }
                     };
                     
                    var res = $http.post(url, $scope.dataObj);
                    res.success(function(data, status, headers, config) {
			$scope.message = data;
                        });
                    res.error(function(data, status, headers, config) {
			alert( "failure message: " + JSON.stringify({data: data}));
                    });
                   
            };
                
            //Delete method in json file
            playerDraftCtrl.removePlayer = function ($index) {
                    url = urltest+"/"+$index;
                    var config = {
                         headers : {
                             'Content-Type': 'application/json;'
                         }
                     };
                     
                    var resDel = $http.delete(url);
                     resDel.success(function(data, status, headers, config) {
			$scope.message = data;
                        //console.log($scope.message);
                        });
                     resDel.error(function(data, status, headers, config) {
                         console.log(data);
                         });
            };
            
                    $scope.isInEditMode = false;
                    $scope.index = -1;
                    $scope.ListOfBookmarks = [];
            
            playerDraftCtrl.editPlayer = function (currObj, index) {
                $scope.isInEditMode = true;
                $scope.ako = angular.copy(currObj);
                $scope.index = index;
                 $scope.dataObj = { 
                     fname: $scope.ako.fname,
                     lname: $scope.ako.lname,
                     salary: $scope.ako.salary,
                     points: $scope.ako.points,
                     rebounds: $scope.ako.rebounds,
                     assists: $scope.ako.assists,
                     steals: $scope.ako.steals,
                     blocks: $scope.ako.blocks
                 }
                console.log($scope.ako);
               $('#myModal').modal('show');

            };
                
            playerDraftCtrl.updatePlayer = function() {   
                    $scope.ListOfBookmarks[$scope.index] = angular.copy($scope.dataObj);
                    $scope.dataObjNew =  $scope.dataObj;
                    console.log("after clicked" + $scope.ako.id);
                    PlayerDraftCtrl.ako.push(); 
                   
                    url = urltest+"/"+$scope.ako.id;
                    var config = {
                         headers : {
                             'Content-Type': 'application/json;'
                         }
                     };
                     
                    var res = $http.put(url, $scope.dataObjNew);
                    res.success(function(data, status, headers, config) {
			$scope.message = data;
                        console.log($scope.message);
                        });
                    res.error(function(data, status, headers, config) {
			alert( "failure message: " + JSON.stringify({data: data}));
                    });
                    
                    console.log($scope.dataObj.fname);
                    $('#myModal').modal('hide');
                };
            
         };  
     
        PlayerDraftApp.service("PlayerSvc", ["$http", "$q", PlayerSvc]);
        PlayerDraftApp.controller("PlayerDraftCtrl", ["$http", "$scope", "PlayerSvc", PlayerDraftCtrl]);
 
})();
